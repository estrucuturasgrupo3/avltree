/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avl;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import tda.AVL;

/**
 *
 * @author joangie
 */
public class Main extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button agregar = new Button("Agregar");        
        Button delete = new Button("Eliminar");
        
        TextField numnA = new TextField();
        TextField numnE = new TextField();
               
        AVL<Integer> arbol = new AVL<>((Integer e1,Integer e2) -> e1-e2);
        Pane contenedor = new Pane();  
        HBox hb= new HBox();
        hb.getChildren().addAll(numnA,agregar,delete,numnE);
        hb.setLayoutX(400);
        hb.setLayoutY(5);
        hb.setSpacing(20);
        contenedor.getChildren().addAll(hb);
        VBox vb = new VBox(5,contenedor,arbol);
        vb.setAlignment(Pos.TOP_CENTER);
        	
                
        
        
       
       // ab.add(5);
        agregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    arbol.add(Integer.parseInt(numnA.getText()));
                }catch(Exception e){
                    mensajeAlerta("Ingrese un dato valido");
                }
                arbol.mostrarArbol();             
                numnA.clear();
            }
        });
    
        delete.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try{
                    arbol.remove(Integer.parseInt(numnE.getText()));
                }catch(Exception e){
                    mensajeAlerta("Ingrese un dato valido");
                }
                arbol.mostrarArbol();
                numnE.clear();
            }
        });
                        
        Scene scene = new Scene(vb,CONSTANTES.MAX_WIDTH,CONSTANTES.MAX_HEIGHT);
        primaryStage.setTitle("Arbol AVL");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    public static void mensajeAlerta(String name){
        Alert alerta= new Alert(Alert.AlertType.INFORMATION);
        alerta.setTitle("oops, un error");
        alerta.setHeaderText(null);
        alerta.setContentText(name);
        alerta.initStyle(StageStyle.UTILITY);
        alerta.showAndWait();
    }
    
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
